# dotfiles
Configs from archlinux + i3 setup

- Note Taking Markdown App : Simplenote $ sncli and snsyc
- Music Player : MPD + Ncmpcpp $ ncmpcpp or cmus
- Mail Client : Neomutt with mutt-wizard
- Terminal : Rxvt Unicode $ urxvt urxvt-perls urxvt-font-size-git
- Browser : Qutebrowser, Google Chrome and Firefox
- System Watch :  $ gotop
- PDF reader : Zathura with ePub extensions
- Image Editors : Feh, imagemagick, sxiv, gthumb
- Raw Editing : Gimp and Darktable
- Calendar - calcurse
- [i3gaps](https://www.archlinux.org/packages/community/x86_64/i3-gaps/)
- [dunst](https://www.archlinux.org/packages/?name=dunst) for notifications
- [bc](https://www.archlinux.org/packages/extra/x86_64/bc/) a simple calendar widget
- [i3blocks](https://aur.archlinux.org/packages/i3blocks) for the status bar
- [dmenu](https://wiki.archlinux.org/index.php/Dmenu) dmenu for calling programs
- [Urxvt](https://wiki.archlinux.org/index.php/urxvt) as the terminal emulator
- [picom](https://wiki.archlinux.org/index.php/Picom) to enable opacity for urxvt
- [mpc](https://www.archlinux.org/packages/?name=mpc) to make media keys work with mpd
- [npm-applet](https://www.archlinux.org/packages/?name=network-manager-applet) for network management
- [redshift](https://www.archlinux.org/packages/?name=redshift) for screen color temperature management
- [feh](https://www.archlinux.org/packages/?name=feh) to set the background
- [qutebrowser](https://aur.archlinux.org/packages/qutebrowser/) as a minimalistic browser
- [playerctl](https://aur.archlinux.org/packages/playerctl/) to make media keys work with spotify
- [caffeine-ng](https://aur.archlinux.org/packages/caffeine-ng/) for caffeine
- [imagemagick](https://www.archlinux.org/packages/?sort=&q=imagemagick) for convert
