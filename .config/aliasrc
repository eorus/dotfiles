#!/usr/bin/env sh

# Aliases

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dir_colors && eval "$(dircolors -b ~/.dir_colors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias bat0='upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|to\ full|percentage"'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias lh='ls -lh'
alias mx='tmuxinator'
alias vim="nvim"
alias oldvim="\vim"
alias pbcopy="xclip -selection clipboard"
alias pbpaste="xclip -selection clipboard -o"
alias home="cd ~"
alias c="clear"
alias h="history"
alias p="cat"
alias pd="pwd"
alias t="date"
alias ka="killall" \
	mkd="mkdir -pv" \
	cfi3="e ~/.config/i3/config" \
	cfal="e ~/.config/aliasrc" \
	edaf="e ~/notes/affiliates.txt" \
	mpv="mpv --input-ipc-server=/tmp/mpvsoc$(date +%s)" \
	calcurse="calcurse -D ~/.calcurse" \
	magit="nvim -c MagitOnly" \
	p="sudo pacman" \
	SS="sudo systemctl" \
	f="$FILE" \
	g="git" \
	trem="transmission-remote" \
	e="$EDITOR" \
	v="$EDITOR" \
	x="sxiv -ft *" \
	sdn="sudo shutdown -h now" \
	gua="git remote | xargs -L1 git push --all" \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi" \
	yt="youtube-dl --add-metadata -i -o '%(upload_date)s-%(title)s.%(ext)s'" \
	yta="yt -x -f bestaudio/best" \
	YT="youtube-viewer" \
	ref="shortcuts >/dev/null; source ~/.config/shortcutrc" \
	upc="sudo pacman -Syu; pkill -RTMIN+8 i3blocks" \
	lsp="pacman -Qett --color=always | less" \
	ffmpeg="ffmpeg -hide_banner"
alias duf='du -hsx * | sort -rh | head -5'
alias treeacl='tree -A -C -L 2'
alias hava='wttr'


command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d" # Use neovim for vim if present.

vf() { fzf | xargs -r -I % $EDITOR % ;}
